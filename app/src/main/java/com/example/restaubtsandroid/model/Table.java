package com.example.restaubtsandroid.model;

public class Table {
    private int tableNr;
    private String reservedName;
    private int isOccupied;
    private int places;

    public Table(int tableNr, String reservedName, int isOccupied, int places) {
        this.tableNr = tableNr;
        this.reservedName = reservedName;
        this.isOccupied = isOccupied;
        this.places = places;
    }

    public int getTableNr() {
        return tableNr;
    }

    public String getReservedName() {
        return reservedName;
    }

    public int getIsOccupied() {
        return isOccupied;
    }

    public int getPlaces() {
        return places;
    }

    @Override
    public String toString() {
        return tableNr + " " + reservedName + " " + isOccupied + " " + places;
    }
}
