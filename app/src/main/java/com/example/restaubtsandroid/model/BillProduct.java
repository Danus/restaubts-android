package com.example.restaubtsandroid.model;

public class BillProduct {
    private int id;
    private String name;
    private Double value;
    private String image;
    private String timestamp;
    private int quantity;

    public BillProduct(int id, String name, Double value, String image, String timestamp, int quantity) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.image = image;
        this.timestamp = timestamp;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getName() {
        return name;
    }

    public Double getValue() {
        return value;
    }

    public String getImage() {
        return image;
    }
}
