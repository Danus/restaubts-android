package com.example.restaubtsandroid.model;

public class Product {
    private int id;
    private String name;
    private String category;
    private Double value;
    private String image;

    public Product(int id, String name, String category, Double value, String image) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.value = value;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public Double getValue() {
        return value;
    }

    public String getImage() {
        return image;
    }
}
