package com.example.restaubtsandroid.model;

public class ApiConfig {
    private String url = "https://restaubts.superbock.fun/api";

    public String getUrl() {
        return url;
    }
}
