package com.example.restaubtsandroid;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.restaubtsandroid.model.ApiConfig;

import java.util.HashMap;
import java.util.Map;

public class ReservationActivity extends AppCompatActivity implements ReserveDialog.ReserveDialogListener {

    private Button reserveButtonImg, reserveButton, cancelRvButtonImg, cancelRvButton, occupiedButtonImg, occupiedButton;
    private int tableNr;
    private String reservation;
    private ReserveDialog reserveDialog;
    private ApiConfig apiConfig = new ApiConfig();
    private SharedPreferences sharedPreferences;

    @Override
    public void setReservationName(String reservationName) {
        reserveDialog.dismiss();
        performRequest("reservation", reservationName);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);

        sharedPreferences = getSharedPreferences("SharedPreferences", MODE_PRIVATE);

        Intent intent = getIntent();
        tableNr = intent.getIntExtra(TablesActivity.EXTRA_NUMBER, 0);
        reservation = intent.getStringExtra(TablesActivity.EXTRA_STRING);

        getSupportActionBar().setTitle("Table " + tableNr);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        reserveButtonImg = (Button) findViewById(R.id.reserveButtonImg);
        reserveButton = (Button) findViewById(R.id.reserveButton);
        cancelRvButtonImg = (Button) findViewById(R.id.cancelRvButtonImg);
        cancelRvButton = (Button) findViewById(R.id.cancelRvButton);
        occupiedButtonImg = (Button) findViewById(R.id.occupiedButtonImg);
        occupiedButton = (Button) findViewById(R.id.occupiedButton);

        // Button Listeners
        reserveButtonImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reserveDialog();
            }
        });
        reserveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reserveDialog();
            }
        });
        cancelRvButtonImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelRvDialog();
            }
        });
        cancelRvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelRvDialog();
            }
        });
        occupiedButtonImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performRequest("occupied", "1");
            }
        });
        occupiedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performRequest("occupied", "1");
            }
        });
    }

    private void reserveDialog() {
        reserveDialog = new ReserveDialog();
        reserveDialog.show(getSupportFragmentManager(), "New Reservation");
    }

    private void cancelRvDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ReservationActivity.this);

        System.out.println(reservation);
        if (reservation.equals(""))  {
            builder.setTitle("Not Reserved")
                    .setMessage("The table is not reserved!")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
        } else {
            builder.setTitle("Cancel Reservation")
                    .setMessage("Do you want to cancel this reservation?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            performRequest("reservation", "");
                        }
                    })
                    .setNegativeButton("No", null);
        }
        builder.create().show();
    }

    private void performRequest(final String type, final String data) {
        RequestQueue queue = Volley.newRequestQueue(ReservationActivity.this);
        String url = apiConfig.getUrl() + "/table/changeStatus";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // Send the user to the table activity
                Intent intent = new Intent(ReservationActivity.this, TablesActivity.class);
                startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.err.println(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tableNr", String.valueOf(tableNr));
                params.put("type", type);
                params.put("data", data);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Authorization", sharedPreferences.getString("accessToken", ""));
                return params;
            }
        };
        queue.add(stringRequest);
    }
}
