package com.example.restaubtsandroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.restaubtsandroid.model.Table;

import java.util.ArrayList;

class TableListAdapter extends ArrayAdapter<Table> {
    private static final String TAG = "TableListAdapter";
    private Context mContext;
    private int mResource;

    static class ViewHolder {
        TextView tableNr;
        TextView places;
        TextView status;
    }

    public TableListAdapter(@NonNull Context context, int resource, ArrayList<Table> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        // Get table Information
        int tableNr = getItem(position).getTableNr();
        String reservedName = getItem(position).getReservedName();
        int isOccupied = getItem(position).getIsOccupied();
        int places = getItem(position).getPlaces();

        ViewHolder holder;

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource, parent, false);

            holder = new ViewHolder();
            holder.tableNr = convertView.findViewById(R.id.tableAdapterTableNr);
            holder.places = convertView.findViewById(R.id.tableAdapterPlaces);
            holder.status = convertView.findViewById(R.id.tableAdapterStatus);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tableNr.setText("Table " + tableNr);
        holder.places.setText(places + " Places");
        holder.status.setText("");
        if (isOccupied == 1) {
            holder.status.setText("Occupied");
        } else if (!reservedName.equals("")){
            holder.status.setText("Rv. " + reservedName);
        }

        return convertView;
    }
}
