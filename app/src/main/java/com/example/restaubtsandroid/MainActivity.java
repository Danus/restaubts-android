package com.example.restaubtsandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View.OnKeyListener;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.restaubtsandroid.model.ApiConfig;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private Button loginButton;
    private EditText username, password;
    private ApiConfig apiConfig = new ApiConfig();
    private TextView invalidCredentialsLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = findViewById(R.id.usernameTextField);
        password = findViewById(R.id.passwordTextField);
        invalidCredentialsLabel = findViewById(R.id.loginFailed);

        loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invalidCredentialsLabel.setVisibility(View.INVISIBLE);
                loginRequest();
            }
        });

        password.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    invalidCredentialsLabel.setVisibility(View.INVISIBLE);
                    loginRequest();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed(){
    }

    private void loginRequest() {
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        String url = apiConfig.getUrl() + "/user/login";

        // Send post parameters with the request
        JSONObject parameters = new JSONObject();
        try {
            //input your API parameters
            parameters.put("username", username.getText().toString());
            parameters.put("password", password.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                // Get Access Token from API
                String accessToken = "";
                try {
                    accessToken = response.getString("access_token");
                } catch (JSONException e) {
                    System.err.println(e);
                }

                // Safe Username and the Access token in shared preferences. So we can access these data on our app.
                SharedPreferences preferences = getSharedPreferences("SharedPreferences", MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("username", username.getText().toString());
                editor.putString("accessToken", "Bearer " + accessToken);
                editor.apply();

                // Send the user to the table activity
                Intent intent = new Intent(MainActivity.this, TablesActivity.class);
                startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.err.println(error);
                invalidCredentialsLabel.setVisibility(View.VISIBLE);
            }
        });

        queue.add(jsonObjectRequest);
    }
}
