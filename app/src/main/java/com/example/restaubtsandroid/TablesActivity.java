package com.example.restaubtsandroid;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.restaubtsandroid.model.ApiConfig;
import com.example.restaubtsandroid.model.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TablesActivity extends AppCompatActivity {

    private ApiConfig apiConfig = new ApiConfig();
    private ArrayList<Table> tableArrayList = new ArrayList<>();
    private ListView tableListView;
    public static final String EXTRA_NUMBER = "com.example.restaubtsandroid.EXTRA_NUMBER";
    public static final String EXTRA_STRING = "com.example.restaubtsandroid.EXTRA_STRING";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tables);
        tableListView = findViewById(R.id.tableListView);

        getSupportActionBar().setTitle("RestauBTS Tables");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getTables();

        tableListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // check if table status changed and send him to next activity
                checkTableStatus(tableArrayList.get(position), position);
            }
        });
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(TablesActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private void getTables() {
        tableArrayList.clear();
        RequestQueue queue = Volley.newRequestQueue(TablesActivity.this);
        String url = apiConfig.getUrl() + "/table/getAll";

        // Get request to get the tables from the api
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // The API will return an Array
                            JSONArray jsonArray = response.getJSONArray("tables");

                            // for each table row, insert the data into my table ArrayList
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject tableObject = jsonArray.getJSONObject(i);

                                int tableNr = tableObject.getInt("idTableNr");
                                String reservedName = tableObject.getString("dtReservedName");
                                int isOccupied = tableObject.getInt("dtIsOccupied");
                                int places = tableObject.getInt("dtPlaces");

                                Table table = new Table(tableNr, reservedName, isOccupied, places);
                                tableArrayList.add(table);
                            }
                        } catch (JSONException e) {
                            System.err.println(e);
                        } finally {
                            // After fetching the data, set the list adapter with the data.
                            TableListAdapter adapter = new TableListAdapter(TablesActivity.this, R.layout.adapter_tables, tableArrayList);
                            tableListView.setAdapter(adapter);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.err.println(error);
            }
        });

        queue.add(request);
    }

    private void checkTableStatus(final Table oldTable, final int position) {
        tableArrayList.clear();
        RequestQueue queue = Volley.newRequestQueue(TablesActivity.this);
        String url = apiConfig.getUrl() + "/table/getAll";

        // Get request to get the tables from the api
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // The API will return an Array
                            JSONArray jsonArray = response.getJSONArray("tables");

                            // for each table row, insert the data into my table ArrayList
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject tableObject = jsonArray.getJSONObject(i);

                                int tableNr = tableObject.getInt("idTableNr");
                                String reservedName = tableObject.getString("dtReservedName");
                                int isOccupied = tableObject.getInt("dtIsOccupied");
                                int places = tableObject.getInt("dtPlaces");

                                Table table = new Table(tableNr, reservedName, isOccupied, places);
                                tableArrayList.add(table);
                            }
                        } catch (JSONException e) {
                            System.err.println(e);
                        } finally {
                            // After fetching the data, set the list adapter with the data.
                            TableListAdapter adapter = new TableListAdapter(TablesActivity.this, R.layout.adapter_tables, tableArrayList);
                            tableListView.setAdapter(adapter);

                            if (tableArrayList.get(position).getIsOccupied() != oldTable.getIsOccupied()) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(TablesActivity.this);

                                builder.setTitle("Changes detected!")
                                        .setMessage("The table occupied status has changed")
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        });

                                builder.create().show();
                            } else if (!tableArrayList.get(position).getReservedName().equals(oldTable.getReservedName())) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(TablesActivity.this);

                                builder.setTitle("Changes detected!")
                                        .setMessage("The table reservation status has changed")
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        });

                                builder.create().show();
                            } else {
                                Intent intent;

                                // If table is occupied, send the user to the category activity, else send the user to the reservation activity
                                if (tableArrayList.get(position).getIsOccupied() == 0) {
                                    intent = new Intent(TablesActivity.this, ReservationActivity.class);
                                    intent.putExtra(EXTRA_STRING, tableArrayList.get(position).getReservedName());
                                } else {
                                    intent = new Intent(TablesActivity.this, CategoryActivity.class);
                                }

                                intent.putExtra(EXTRA_NUMBER, tableArrayList.get(position).getTableNr());
                                startActivity(intent);
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.err.println(error);
            }
        });

        queue.add(request);
    }
}
