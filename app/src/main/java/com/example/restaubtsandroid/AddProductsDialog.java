package com.example.restaubtsandroid;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatDialogFragment;

public class AddProductsDialog extends AppCompatDialogFragment {
    private EditText quantityInput;
    private AddProductDialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_quantity, null);

        quantityInput = view.findViewById(R.id.productQuantity);

        // on enter call listener
        quantityInput.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    try {
                        int quantity = Integer.valueOf(quantityInput.getText().toString());
                        listener.setProductQuantity(quantity);
                    } catch (NumberFormatException e) {
                        listener.setProductQuantity(0);
                    }
                    return true;
                }
                return false;
            }
        });

        builder.setView(view)
                .setTitle("Add Product")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                })
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            int quantity = Integer.valueOf(quantityInput.getText().toString());
                            listener.setProductQuantity(quantity);
                        } catch (NumberFormatException e) {
                            listener.setProductQuantity(0);
                        }
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // force implementation !
        try {
            listener = (AddProductDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement AddProductDialogListener");
        }
    }

    public interface AddProductDialogListener {
        void setProductQuantity(int quantityInput);
    }
}
