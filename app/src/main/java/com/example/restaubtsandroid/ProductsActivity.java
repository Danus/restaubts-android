package com.example.restaubtsandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.restaubtsandroid.model.ApiConfig;
import com.example.restaubtsandroid.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProductsActivity extends AppCompatActivity implements AddProductsDialog.AddProductDialogListener {

    private int tableNr;
    private String category;
    private ApiConfig apiConfig = new ApiConfig();
    private ArrayList<Product> productArrayList = new ArrayList<>();
    private ListView productListView;
    private EditText searchField;
    private AddProductsDialog addProductsDialog;
    private int currentProductID;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        productListView = findViewById(R.id.productListView);
        searchField = findViewById(R.id.productSearchField);

        searchField.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                 if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // If the event is a key-down event on the "enter" button
                    getProducts(searchField.getText().toString());
                    return true;
                }
                return false;
            }
        });

        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                System.out.println(s);
                if (s.length() == 0) {
                    getProducts("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        Intent intent = getIntent();
        tableNr = intent.getIntExtra(TablesActivity.EXTRA_NUMBER, 0);
        category = intent.getStringExtra(CategoryActivity.EXTRA_STRING);

        sharedPreferences = getSharedPreferences("SharedPreferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("tableNr", tableNr);
        editor.apply();

        getSupportActionBar().setTitle("Table " + tableNr);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getProducts("");

        productListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currentProductID = productArrayList.get(position).getId();
                addProductsDialog = new AddProductsDialog();
                addProductsDialog.show(getSupportFragmentManager(), "Enter Quantity");
            }
        });
    }

    private void getProducts(String search) {
        productArrayList.clear();

        RequestQueue queue = Volley.newRequestQueue(ProductsActivity.this);
        String url = apiConfig.getUrl() + "/product/getByCategory/" + category + "/" + search;

        // Get request to get the tables from the api
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // The API will return an Array
                            JSONArray jsonArray = response.getJSONArray("products");

                            // for each table row, insert the data into my table ArrayList
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject productObject = jsonArray.getJSONObject(i);

                                int id = productObject.getInt("idProduct");
                                String name = productObject.getString("dtName");
                                String category = productObject.getString("dtCategory");
                                Double value = productObject.getDouble("dtValue");
                                String image = productObject.getString("dtImage");

                                Product product = new Product(id, name, category, value, image);
                                productArrayList.add(product);
                            }
                        } catch (JSONException e) {
                            System.err.println(e);
                        } finally {
                            // After fetching the data, set the list adapter with the data.
                            ProductListAdapter adapter = new ProductListAdapter(ProductsActivity.this, R.layout.adapter_products, productArrayList);
                            productListView.setAdapter(adapter);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.err.println(error);
            }
        });

        queue.add(request);
    }

    private void performRequest(final int quantityInput) {
        RequestQueue queue = Volley.newRequestQueue(ProductsActivity.this);
        String url = apiConfig.getUrl() + "/bill/addProduct";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(ProductsActivity.this, "Product added!", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.err.println(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tableNr", String.valueOf(tableNr));
                params.put("productId", String.valueOf(currentProductID));
                params.put("quantity", String.valueOf(quantityInput));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Authorization", sharedPreferences.getString("accessToken", ""));
                return params;
            }
        };
        queue.add(stringRequest);
    }

    @Override
    public void setProductQuantity(int quantityInput) {
        addProductsDialog.dismiss();
        if (quantityInput == 0) {

            AlertDialog.Builder builder = new AlertDialog.Builder(ProductsActivity.this);
            builder.setTitle("Error")
                    .setMessage("The Product has not been added. Please check your Input!")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

            builder.create().show();
        } else {
            performRequest(quantityInput);
        }
    }
}
