package com.example.restaubtsandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CategoryActivity extends AppCompatActivity {

    private Button appetizers, mainDishes, menus, desserts, coldDrinks, warmDrinks, bill;
    private int tableNr;
    private SharedPreferences sharedPreferences;
    public static final String EXTRA_NUMBER = "com.example.restaubtsandroid.EXTRA_NUMBER";
    public static final String EXTRA_STRING = "com.example.restaubtsandroid.EXTRA_STRING";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        sharedPreferences = getSharedPreferences("SharedPreferences", MODE_PRIVATE);

        Intent intent = getIntent();
        tableNr = intent.getIntExtra(TablesActivity.EXTRA_NUMBER, 0);

        if (tableNr == 0) {
            tableNr = sharedPreferences.getInt("tableNr", 0);
        }

        getSupportActionBar().setTitle("Table " + tableNr);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        appetizers = (Button) findViewById(R.id.appetizers);
        mainDishes = (Button) findViewById(R.id.mainDishes);
        menus = (Button) findViewById(R.id.menus);
        desserts = (Button) findViewById(R.id.desserts);
        coldDrinks = (Button) findViewById(R.id.coldDrinks);
        warmDrinks = (Button) findViewById(R.id.warmDrinks);
        bill = (Button) findViewById(R.id.bill);

        appetizers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToProductsActivity("Appetizers");
            }
        });
        mainDishes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToProductsActivity("Main Dishes");
            }
        });
        menus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToProductsActivity("Menus");
            }
        });
        desserts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToProductsActivity("Desserts");
            }
        });
        coldDrinks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToProductsActivity("Cold Drinks");
            }
        });
        warmDrinks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToProductsActivity("Hot Drinks");
            }
        });
        bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CategoryActivity.this, BillActivity.class);
                intent.putExtra(EXTRA_NUMBER, tableNr);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(CategoryActivity.this, TablesActivity.class);
        startActivity(intent);
    }

    private void goToProductsActivity(String category) {
        Intent intent = new Intent(CategoryActivity.this, ProductsActivity.class);
        intent.putExtra(EXTRA_NUMBER, tableNr);
        intent.putExtra(EXTRA_STRING, category);
        startActivity(intent);
    }
}
