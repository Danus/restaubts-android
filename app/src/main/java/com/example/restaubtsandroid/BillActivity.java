package com.example.restaubtsandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.example.restaubtsandroid.model.ApiConfig;
import com.example.restaubtsandroid.model.BillProduct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BillActivity extends AppCompatActivity implements EditBillProductsDialog.EditProductDialogListener {

    private int tableNr;
    private ApiConfig apiConfig = new ApiConfig();
    private ArrayList<BillProduct> productArrayList = new ArrayList<>();
    private SwipeMenuListView billListView;
    private EditText searchField;
    private EditBillProductsDialog editBillProductsDialog;
    private int currentProductID;
    private String currentProductTimestamp;
    private SharedPreferences sharedPreferences;
    private TextView billTotalTextView;
    private Double billTotal;
    private Button finishServiceButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill);

        billListView = (SwipeMenuListView) findViewById(R.id.billListView);
        initSwipeMenu();

        searchField = findViewById(R.id.billSearchField);
        billTotalTextView = findViewById(R.id.billTotal);
        finishServiceButton = findViewById(R.id.billEndService);

        finishServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(BillActivity.this);
                builder.setTitle("Finish Service")
                        .setMessage("Do you want finish the service? After this you will not be able to modify this bill!")
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {}
                        })
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                performFinishServiceRequest();
                            }
                        });

                builder.create().show();
            }
        });

        searchField.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // If the event is a key-down event on the "enter" button
                    getProducts(searchField.getText().toString());
                    return true;
                }
                return false;
            }
        });

        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    getProducts("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        Intent intent = getIntent();
        tableNr = intent.getIntExtra(TablesActivity.EXTRA_NUMBER, 0);

        sharedPreferences = getSharedPreferences("SharedPreferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("tableNr", tableNr);
        editor.apply();

        getSupportActionBar().setTitle("Table " + tableNr);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getProducts("");

        billListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currentProductID = productArrayList.get(position).getId();
                currentProductTimestamp = productArrayList.get(position).getTimestamp();
                editBillProductsDialog = new EditBillProductsDialog();
                editBillProductsDialog.show(getSupportFragmentManager(), "Enter Quantity");
            }
        });
    }

    private void initSwipeMenu() {
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // Create delete menuoption
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                deleteItem.setWidth(180);
                deleteItem.setTitle("Delete");
                deleteItem.setTitleColor(Color.WHITE);
                deleteItem.setTitleSize(18);

                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        billListView.setMenuCreator(creator);

        billListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                AlertDialog.Builder builder = new AlertDialog.Builder(BillActivity.this);
                builder.setTitle("Delete Product")
                        .setMessage("Do you want to delete this Product?")
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {}
                        })
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                performDeleteRequest(productArrayList.get(position).getId(), productArrayList.get(position).getTimestamp());
                            }
                        });

                builder.create().show();
                return false;
            }
        });
    }

    private void getProducts(String search) {
        productArrayList.clear();
        billTotal = 0.0;

        RequestQueue queue = Volley.newRequestQueue(BillActivity.this);
        String url = apiConfig.getUrl() + "/product/getByTable/" + tableNr + "/" + search;

        // Get request to get the tables from the api
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // The API will return an Array
                            JSONArray jsonArray = response.getJSONArray("products");

                            // for each table row, insert the data into my table ArrayList
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject productObject = jsonArray.getJSONObject(i);

                                int id = productObject.getInt("idProduct");
                                String name = productObject.getString("dtName");
                                Double value = productObject.getDouble("dtValue");
                                String image = productObject.getString("dtImage");
                                String timeStamp = productObject.getString("id");
                                int quantity = productObject.getInt("dtQuantity");

                                billTotal = Double.sum(billTotal, value * quantity);

                                BillProduct product = new BillProduct(id, name, value, image, timeStamp, quantity);
                                productArrayList.add(product);
                            }
                        } catch (JSONException e) {
                            System.err.println(e);
                        } finally {
                            billTotalTextView.setText("Total: " + String.format("%.2f", billTotal) + "€");
                            // After fetching the data, set the list adapter with the data.
                            BillListAdapter adapter = new BillListAdapter(BillActivity.this, R.layout.adapter_bill, productArrayList);
                            billListView.setAdapter(adapter);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.err.println(error);
            }
        });

        queue.add(request);
    }

    private void performFinishServiceRequest() {
        RequestQueue queue = Volley.newRequestQueue(BillActivity.this);
        String url = apiConfig.getUrl() + "/table/changeStatus";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // Send the user to the table activity
                Intent intent = new Intent(BillActivity.this, TablesActivity.class);
                startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.err.println(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tableNr", String.valueOf(tableNr));
                params.put("type", "serviceFinished");
                params.put("data", "");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Authorization", sharedPreferences.getString("accessToken", ""));
                return params;
            }
        };
        queue.add(stringRequest);
    }

    private void performModifyRequest(final int quantityInput) {
        RequestQueue queue = Volley.newRequestQueue(BillActivity.this);
        String url = apiConfig.getUrl() + "/bill/editProduct";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(BillActivity.this, "Product modified!", Toast.LENGTH_SHORT).show();
                getProducts(searchField.getText().toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.err.println(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tableNr", String.valueOf(tableNr));
                params.put("productId", String.valueOf(currentProductID));
                params.put("id", currentProductTimestamp);
                params.put("quantity", String.valueOf(quantityInput));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Authorization", sharedPreferences.getString("accessToken", ""));
                return params;
            }
        };
        queue.add(stringRequest);
    }

    private void performDeleteRequest(final int id, final String timestamp) {
        RequestQueue queue = Volley.newRequestQueue(BillActivity.this);
        String url = apiConfig.getUrl() + "/bill/deleteProduct";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(BillActivity.this, "Product deleted!", Toast.LENGTH_SHORT).show();
                getProducts(searchField.getText().toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.err.println(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("tableNr", String.valueOf(tableNr));
                params.put("productId", String.valueOf(id));
                params.put("id", timestamp);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("Authorization", sharedPreferences.getString("accessToken", ""));
                return params;
            }
        };
        queue.add(stringRequest);
    }

    @Override
    public void setProductQuantity(int quantityInput) {
        editBillProductsDialog.dismiss();
        if (quantityInput == 0) {

            //If input from dialog is not valid, show this error message
            AlertDialog.Builder builder = new AlertDialog.Builder(BillActivity.this);
            builder.setTitle("Error")
                    .setMessage("The Product has not been modified. Please check your Input!")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

            builder.create().show();
        } else {
            performModifyRequest(quantityInput);
        }
    }
}
